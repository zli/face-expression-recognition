#face expression recognition

为了测试方便，对图像进行了采点处理

![sampling][sampling]

Gabor滤波结果

![gabor_test][gabor_test]

PCA测试结果

![pca_test][pca_test]

LDA测试结果

![lda_test][lda_test]

主界面

![mainform][mainform]

训练结果

![train_result][train_result]

测试结果

![test_result][test_result]

[sampling]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/sampling.jpg

[gabor_test]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/gabor_test.png

[pca_test]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/pca_test.png

[lda_test]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/lda_test.png

[mainform]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/mainform.png

[train_result]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/train_result.png

[test_result]:http://git.oschina.net/windstyle/face-expression-recognition/raw/master/images/test_result.png